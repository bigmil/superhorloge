#!/usr/bin/env python3
"""
Programme python3/tkinter qui affiche une montre et les éphémérides pour un lieu donné
Permet de définir une alarme
Permet de visualiser une série de photos dans un canvas
"""

import configparser

#get path of the script and instert it in the sys.path, must be done for loading eeclock package
import sys, os
path = os.path.dirname(sys.argv[0])
path = os.path.abspath(path)
sys.path.insert(0, path+'/..')

from eeclock.graphics import MainWindow, Frame, Button, Notebook
from eeclock.clock import Clock
from eeclock.ephemerides import Ephemerides
from eeclock.alarm import Alarm
from eeclock.config import Config

# config file name with path
configfile = path+'/../resources/config.txt'

config = Config(configfile)

TK = config['TK']

# création de la fenêtre principale
mw = MainWindow(TK['width'], TK['height'], 'Horloge avec éphémérides')

# création de la fenêtre de gauche pour l'horloge
LeftFrame = Frame(mw, 'left', 'nw') 

# création de la fenêtre de droite pour les éphémérides, les notebooks et les boutons
RightFrame = Frame(mw, 'left', 'nw')

# création de la fenêtre contenant les éphémérides
EphemFrame = Frame(RightFrame, 'top', 'nw')

# création de la frame des notebook
NotebookFrame = Frame(RightFrame, 'top', 'nw')

# création dee l'horloge avec actualisation
MyClock = Clock(LeftFrame, config)
MyClock.run_clock()
# création des éphémérides
MyEphem = Ephemerides(EphemFrame, config)
MyEphem.run_ephem()

#création des notebooks
(Notebooks, alarmtab) = Notebook(mw, NotebookFrame, 'top', 'nw')

alarm1 = Alarm(alarmtab, 'Alarme 1 : ', MyClock)

mw.overrideredirect(1)# removing the top bar with control of the X-window

mw.mainloop()
