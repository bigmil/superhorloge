import tkinter
import tkinter.ttk
import configparser
import math
import playsound
import time
import sys, os

class Clock:
    """
    Gestion de la montre sur un canvas
    """

    alarm = dict()

    def __init__(self, mw, config):
        """
        Initialisation de la montre avec ses différents objets
        """
        # définition des paramètres de l'horloge
        CLOCK = config['CLOCK']
        self.width = int(CLOCK['width'])
        self.height = int(CLOCK['height'])
        self.background = CLOCK['background']
        self.border = int(CLOCK['border'])
        self.ring_width = int(CLOCK['ring_width'])
        self.ring_color = CLOCK['ring_color']
        self.ext_ring_shift = int(self.ring_width/2)
        self.center_ring_width = int(CLOCK['center_ring_width'])
        self.center_ring_color = CLOCK['center_ring_color']
        self.wph_color = CLOCK['wph_color']
        self.wph_width = int(CLOCK['wph_width'])
        self.wph_length = int(CLOCK['wph_length'])
        self.wpm_color = CLOCK['wpm_color']
        self.wpm_width = int(CLOCK['wpm_width'])
        self.wpm_length = int(CLOCK['wpm_length'])
        self.wps_color = CLOCK['wps_color']
        self.wps_width = int(CLOCK['wps_width'])
        self.wps_length = int(CLOCK['wps_length'])
        self.tick1_width = int(CLOCK['tick1_width'])
        self.tick1_color = CLOCK['tick1_color']
        self.tick5_width = int(CLOCK['tick5_width'])
        self.tick5_color = CLOCK['tick5_color']
        self.tick15_width = int(CLOCK['tick15_width'])
        self.tick15_color = CLOCK['tick15_color']

         # création du canvas de l'horloge
        self.clock = tkinter.Canvas(mw, width=self.width, height=self.height, bg=self.background)
        # définition du centre du canvas horloge
        self.xcc = int(self.width / 2) # centre de l'horloge X
        self.ycc = int(self.height / 2) # centre de l'horloge Y
        # création des aiguilles
        self.wph = self.clock.create_line(self.xcc,
                                          self.ycc,
                                          self.xcc, 1,
                                          fill=self.wph_color,
                                          width=self.wph_width) # hour
        self.wpm = self.clock.create_line(self.xcc,
                                          self.ycc,
                                          self.xcc, 1,
                                          fill=self.wpm_color,
                                          width=self.wpm_width) # min
        self.wps = self.clock.create_line(self.xcc,
                                          self.ycc,
                                          self.xcc, 1,
                                          fill=self.wps_color,
                                          width=self.wps_width) # sec
        # création de l'anneau extérieur
        self.clock.create_oval(self.border,
                               self.border,
                               self.width-self.border,
                               self.height-self.border,
                               fill='',
                               width=self.ring_width,
                               outline=self.ring_color)
        self.clock.create_oval(self.border-self.ext_ring_shift,
                               self.border-self.ext_ring_shift,
                               self.width-self.border+self.ext_ring_shift,
                               self.height-self.border+self.ext_ring_shift,
                               fill='', width=1)
        # création du centre de l'horloge
        self.clock.create_oval(int(self.width/2)-self.center_ring_width,
                               int(self.height/2)-self.center_ring_width,
                               int(self.width/2)+self.center_ring_width,
                               int(self.height/2)+self.center_ring_width,
                               fill=self.center_ring_color,
                               outline='black',
                               width=1,
                               tag="axe")
        # création des marques pour les minutes, 5 minutes et 15 minutes sur l'anneau extérieur.
        i = 1
        while i < 61:
            j = i*((2*math.pi)/60)
            x = (math.cos(j)*self.width/2.1)+self.width/2
            y = (math.sin(j)*self.height/2.1)+self.height/2
            if i in (15, 30, 45, 60):
                self.clock.create_oval(x-self.tick15_width,
                                       y-self.tick15_width,
                                       x+self.tick15_width,
                                       y+self.tick15_width,
                                       fill=self.tick15_color)
            elif i in (5, 10, 20, 25, 35, 40, 50, 55):
                self.clock.create_oval(x-self.tick5_width,
                                       y-self.tick5_width,
                                       x+self.tick5_width,
                                       y+self.tick5_width,
                                       fill=self.tick5_color)
            else:
                self.clock.create_oval(x-self.tick1_width,
                                       y-self.tick1_width,
                                       x+self.tick1_width,
                                       y+self.tick1_width,
                                       fill=self.tick1_color)
            i = i+1
        # pack le canvas clock dans la fenêtre principale
        self.clock.pack(side='left', anchor='w')


    def run_clock(self):
        """
        Fonction utilisée pour actualiser la montre
        """
        # obtention des heures, minutes et secondes à partir du temps local
        now = time.localtime()
        hour = now[3]
        minu = now[4]
        sec = now[5]
        # soustrait 12 heures après midi, l'horloge analogique n'a que 12 H
        if hour > 12:
            hour = hour -12

        # affiche heures
        hour = hour+(minu/60.00)
        j = (hour+9)*((2*math.pi)/12)
        x = (math.cos(j)*self.wph_length)+self.xcc
        y = (math.sin(j)*self.wph_length)+self.ycc
        self.clock.coords(self.wph, self.xcc, self.ycc, x, y)
        # affiche minutes
        minu = minu+(sec/60.0)
        j = (minu+45)*((2*math.pi)/60)
        x = (math.cos(j)*self.wpm_length)+self.xcc
        y = (math.sin(j)*self.wpm_length)+self.ycc
        self.clock.coords(self.wpm, self.xcc, self.ycc, x, y)
        # affiche secondes
        j = (sec+45)*((2*math.pi)/60)
        x = (math.cos(j)*self.wps_length)+self.xcc
        y = (math.sin(j)*self.wps_length)+self.ycc
        self.clock.coords(self.wps, self.xcc, self.ycc, x, y)

        if 'alarme1' in self.alarm.keys():
            alahour = self.alarm['alarme1'].alval['hour']
            alamin = self.alarm['alarme1'].alval['minute']
            alastate = self.alarm['alarme1'].alval['state']

            if str(now[3]) == str(alahour) and str(now[4]) == str(alamin) and alastate :
            #Beep(800,100)
                playsound.playsound('/home/dom/python/superhorloge/resources/alarm1.wav')

        # réactualise la montre pour faire tourner les aiguilles.
        self.clock.after(999, self.run_clock)

    def init_alarm(self, alarm, alarmname):
        self.alarm[alarmname] = alarm
        print('{} {} {} {}'.format(alarmname, self.alarm[alarmname].alval['hour'], alarm.alval['minute'], alarm.alval['state']))

