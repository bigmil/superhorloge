import tkinter
import tkinter.ttk
import configparser
import ephem
import time
import locale
import ast
import sys, os

from datetime import datetime

# definition of locale parameter
locale.setlocale(locale.LC_ALL, 'fr_CH.UTF-8')

class Ephemerides:
    '''Classe de calcul des éphémérides'''


    def __init__(self, frame, config):
        '''Initialisation de éphémérides
        1 paramètre: la frame qui doit contenir les éphémérides
        '''
        EPHEM = config['EPHEM']
        self.width = int(EPHEM['width'])
        self.height = int(EPHEM['height'])
        self.background = EPHEM['background']
        self.months = EPHEM['months']
        self.origx = int(EPHEM['origx'])
        self.origy = int(EPHEM['origy'])
        self.spacey = int(EPHEM['spacey'])
        self.fontcol = EPHEM['fontcol']
        self.fonttitle = EPHEM['fonttitle']
        self.fontdatetime = EPHEM['fontdatetime']
        self.font1 = EPHEM['font1']
        self.font2 = EPHEM['font2']
        self.lat = EPHEM['lat']
        self.lon = EPHEM['lon']
        self.elevation = int(EPHEM['elevation'])

        self.location = ephem.Observer()
        self.location.lat = self.lat
        self.location.lon = self.lon
        self.location.elevation = self.elevation
        # création du canvas contenant les éphémérides
        self.ephem_display = tkinter.Canvas(frame,
                                            width=self.width,
                                            height=self.height,
                                            bg=self.background)
        self.ephem_display.pack(side='top', anchor='nw')
        #création du titre
        self.ephem_display.create_text(int(self.width)/2,
                                       self.origy,
                                       fill=self.fontcol,
                                       font=self.fonttitle,
                                       anchor='c',
                                       text="Éphémérides Genève")
        # création de la date et de l'heure
        self.nd = self.ephem_display.create_text(int(self.width)/2,
                                                 self.origy+2*self.spacey,
                                                 fill=self.fontcol,
                                                 font=self.font1,
                                                 anchor='c') # date
        self.nt = self.ephem_display.create_text(int(self.width)/2,
                                                 self.origy+4*self.spacey,
                                                 fill=self.fontcol,
                                                 font=self.fontdatetime,
                                                 anchor='c') # time
        # création des éphémérides du soleil
        self.ephem_display.create_text(self.origx,
                                       self.origy+6*self.spacey,
                                       fill=self.fontcol,
                                       font=self.font1,
                                       anchor='w',
                                       text="Soleil")
        self.sr = self.ephem_display.create_text(self.origx,
                                                 self.origy+7*self.spacey,
                                                 fill=self.fontcol,
                                                 font=self.font2,
                                                 anchor='w') # sun rise
        self.sz = self.ephem_display.create_text(self.origx,
                                                 self.origy+8*self.spacey,
                                                 fill=self.fontcol,
                                                 font=self.font2,
                                                 anchor='w') # sun zenith
        self.ss = self.ephem_display.create_text(self.origx,
                                                 self.origy+9*self.spacey,
                                                 fill=self.fontcol,
                                                 font=self.font2,
                                                 anchor='w') # sun set
        self.dd = self.ephem_display.create_text(self.origx,
                                                 self.origy+10*self.spacey,
                                                 fill=self.fontcol,
                                                 font=self.font2,
                                                 anchor='w') # duration of day
        # création des éphémérides de la lune
        self.ephem_display.create_text(self.origx,
                                       self.origy+12*self.spacey,
                                       fill=self.fontcol,
                                       font=self.font1,
                                       anchor='w',
                                       text="Lune")
        self.mr = self.ephem_display.create_text(self.origx,
                                                 self.origy+13*self.spacey,
                                                 fill=self.fontcol,
                                                 font=self.font2,
                                                 anchor='w') # moon rise
        self.ms = self.ephem_display.create_text(self.origx,
                                                 self.origy+14*self.spacey,
                                                 fill=self.fontcol,
                                                 font=self.font2,
                                                 anchor='w') # moon set
        self.nm = self.ephem_display.create_text(self.origx,
                                                 self.origy+15*self.spacey,
                                                 fill=self.fontcol,
                                                 font=self.font2,
                                                 anchor='w') # last new moon
        self.fm = self.ephem_display.create_text(self.origx,
                                                 self.origy+16*self.spacey,
                                                 fill=self.fontcol,
                                                 font=self.font2,
                                                 anchor='w') # nest full moon
        self.pcm = self.ephem_display.create_text(self.origx,
                                                  self.origy+17*self.spacey,
                                                  fill=self.fontcol,
                                                  font=self.font2,
                                                  anchor='w') # percentage of moon illuminated

    def run_ephem(self):
        '''Fonction pour l'actualisation des éphémérides'''
        # calcule la date et heure actuelle
        now = time.strftime('%H:%M:%S') # Heure, minute, seconde
        today = time.strftime('%Y/%m/%d') # Année, mois, jour
        todaystr = time.strftime('%A %d %B %Y') # Jour de la semaine selon la Locale, Jour du mois, Mois selon la Locale, Année 4 chiffres.

        # éphémérides du soleil
        (leverS, zenithS, coucherS, zenithSalt, diff, difftyms) = self.sunephem(self.location, today)

        # éphémérides de la lune
        #utcnow = datetime.strftime(datetime.utcnow(), "%Y/%m/%d %H:%M:%S")
        (leverL, coucherL, msi, nnm, nfm) = self.moonephem(self.location, today)

        self.ephem_display.itemconfigure(self.nd, text="{}".format(todaystr))
        self.ephem_display.itemconfigure(self.nt, text="{}".format(now))
        self.ephem_display.itemconfigure(self.sr, text="Lever : {}".format(leverS))
        self.ephem_display.itemconfigure(self.sz, text="Zénith : {}, altitude : {}°".format(zenithS, zenithSalt))
        self.ephem_display.itemconfigure(self.ss, text="Coucher : {}".format(coucherS))
        self.ephem_display.itemconfigure(self.dd, text="Durée du jour : {} ({})".format(diff, difftyms))
        self.ephem_display.itemconfigure(self.mr, text="Lever : {}".format(leverL))
        self.ephem_display.itemconfigure(self.ms, text="Coucher : {}".format(coucherL))
        self.ephem_display.itemconfigure(self.nm, text="Nouvelle lune : {}".format(nnm))
        self.ephem_display.itemconfigure(self.fm, text="Pleine lune : {}".format(nfm))
        self.ephem_display.itemconfigure(self.pcm, text="Surface éclairée : {} %".format(msi))
        self.ephem_display.after(999, self.run_ephem)

    def sunephem(self, loc, date):
        '''
        calcule les éphémérides du soleil et les renvoie
        2 arguments :
            ephem.loc
            date (format YYYY/MM/DD)
        '''

        loc.date = date
        sun = ephem.Sun() # calcul des éphémérides pour le jour "date"
        rise = ephem.localtime(loc.next_rising(sun)) # lever du soleil, lt
        zenithUTC = loc.next_transit(sun) # zenith (date/time UTC)
        zenith = ephem.localtime(loc.next_transit(sun)) # zenith du soleil, lt
        sunset = ephem.localtime(loc.next_setting(sun)) # coucher du soleil, lt
        # jour "date"
        t1 = datetime.strptime(str(rise)[0:19], '%Y-%m-%d %H:%M:%S')
        t2 = datetime.strptime(str(sunset)[0:19], '%Y-%m-%d %H:%M:%S')
        diff = t2-t1 # longueur de la journée en HH:MM:SS
        dls = int(str(diff)[0:2])*3600 + int(str(diff)[3:5])*60 + int(str(diff)[6:8]) # longueur de la journée en secondes
        # jour "date -1"
        rise_previous = ephem.localtime(loc.previous_rising(sun)) # lever du soleil, lt
        sunset_previous = ephem.localtime(loc.previous_setting(sun)) # coucher du soleil, lt
        t3 = datetime.strptime(str(rise_previous)[0:19], '%Y-%m-%d %H:%M:%S')
        t4 = datetime.strptime(str(sunset_previous)[0:19], '%Y-%m-%d %H:%M:%S')
        diff_p = t4-t3 # longueur de la journée en HH:MM:SS
        dls_p = int(str(diff_p)[0:2])*3600 + int(str(diff_p)[3:5])*60 + int(str(diff_p)[6:8]) # longueur de la journée en secondes
        # calcul de la différence entre "date" et "date -1" en minutes et secondes
        delta = int(dls - dls_p)
        if delta <= 0:
            dm = divmod(abs(delta), 60)
            diffdaylength = '-{}\'{}"'.format(dm[0], dm[1])
        else:
            dm = divmod(delta, 60)
            diffdaylength = '+{}\'{}"'.format(dm[0], dm[1])
        loc.date = zenithUTC
        sunt = ephem.Sun(loc)
        zenithalt = '{:.0f}'.format(float(sunt.alt)* 57.2957795) # conversion en degrés et arrondi à .0f
        return(str(rise)[11:19], str(zenith)[11:19], str(sunset)[11:19], zenithalt, diff, diffdaylength)

    def moonephem(self, loc, date):
        '''
        calcule les éphémérides de la lune et les renvoie
        2 arguments :
            ephem.loc
            date (format YYYY/MM/DD HH:MM:SS)
        '''

        loc.date = date
        # éphémérides de la lune
        moon = ephem.Moon()
        leverL = ephem.localtime(loc.next_rising(moon))
        coucherL = ephem.localtime(loc.next_setting(moon))
        nfm = ephem.localtime(ephem.next_full_moon(date))
        nnm = ephem.localtime(ephem.next_new_moon(date))
        utcnow = datetime.strftime(datetime.utcnow(), "%Y/%m/%d %H:%M:%S")
        loc.date = utcnow # pour le calcul de la msi on prend now et pas 00:00:00
        msi = moon.moon_phase

        return(str(leverL)[11:19], str(coucherL)[11:19], '{:.0f}'.format(msi*100), str(nnm)[0:19], str(nfm)[0:19])
