import tkinter
import tkinter.ttk

class MainWindow:
    ''' Génère une fenêtre principale tkinter'''

    def __new__(cls, width, height, title):
        '''
        Initialisation de la mainwindow
        3 arguments :
            width: largeur de la fenêtre en pixel
            height: hauteur de la fenêtre en pixel
            title: titre de la fenêtre
        '''
        # création de la frame tk (fenêtre principale)
        mw = tkinter.Tk(className=title)
        mw.geometry(width+"x"+height)
        mw.resizable(width=False, height=False)
        return mw

class Frame:
    '''
    Génère une fenêtre (frame) dans une mainwindow
        3 arguments :
            mainwindow: la fenêtre principale
            side: l'option side de pack
            anchor: l'option anchor de pack
    '''
    def __new__(cls, mainwindow, side, anchor):
        frame = tkinter.Frame(mainwindow) 
        frame.pack(side=side, anchor=anchor)
        return(frame)

class Button:
    '''
    Genère un bouton
        3 arguments :
            frame: la frame qui contient le bouton
            textB: le texte (label) du bouton
            commandB: la commande executée par le bouton
            row: l'option row de grid
            column = l'option column de grid
    '''
    def __new__(cls, frame, textB, commandB, row, column):
        button = tkinter.Button(frame, text=textB, command=commandB).grid(row=row, column=column)
        return(button)

class Notebook:
    """
    Génère un notebook avec 3 onglets
    4 arguments :
        mainwindow: la fenêtre principale
        frame: le contenant le notebook
        side: l'option side de pack
        anchor: l'option anchor de pack
    """
    def __new__(cls, mainwindow, frame, side, anchor):
        note = tkinter.ttk.Notebook(frame, padding=10)

        alarmtab = tkinter.ttk.Frame(note)
        timertab = tkinter.ttk.Frame(note)
        quittab = tkinter.ttk.Frame(note)

        note.add(alarmtab, text = "Alarme")
        note.add(timertab, text = "Timer")
        note.add(quittab, text = "Quitter")

        Button(quittab, 'Quitter', lambda:mainwindow.destroy(), 0, 0)
        note.pack(side = side, anchor = anchor)
        return(note, alarmtab)
