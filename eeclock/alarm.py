import tkinter
import tkinter.ttk
import time

class Alarm:
    """
    Bouton d'alarme pour l'horloge
    """
    isbottom = False
    alval = dict() # initialisation of dict alarm1  for values, hour and minutes

    def __init__(self, frame, texte, clock):
        self.myclock = clock
        alarm = tkinter.Button(frame, text=texte+"{}:{}".format('-', '-'))
        coloralarm = alarm.cget('background')
        self.alval['hour'] = '-'
        self.alval['minute'] = '-'
        self.alval['state'] = False
        alarm.configure(command=lambda: self.set_alarm(frame, alarm, coloralarm))
        alarm.grid(row=0, column=0)
        
    def set_alarm(self, frame, myalarm, colorButton):
        """
        Gestion des tâches du bouton alarm
        """
        if self.isbottom:
            pass
        else:
            now = time.localtime()
            nowh = now[3]
            nowm = now[4]

            self.isbottom = True
        
            (tkinter.ttk.Label(frame, text='Heures')).grid(row=1, column=0)
        
            choicesH = []
            for x in range(24):
                choicesH.append(x)
            hour = tkinter.ttk.Combobox(frame, values=choicesH)
            hour.current(choicesH.index(nowh))
            hour.grid(row=1, column=0)
        
            (tkinter.ttk.Label(frame, text='Minutes')).grid(row=1, column=1)
        
            choicesM = []
            for x in range(60):
                choicesM.append(x)
            minute = tkinter.ttk.Combobox(frame, values=choicesM)
            minute.current(choicesM.index(nowm))
            minute.grid(row=1, column=1)
        
            cancelB = tkinter.Button(frame,
                                     text='Annuler',
                                     command= lambda: self.close_alarm(myalarm,
                                                                       '-',
                                                                       '-',
                                                                       colorButton,
                                                                       False))
            cancelB.grid(row=2, column=0)
            saveB = tkinter.Button(frame,
                                   text='Sauver',
                                   command= lambda: self.close_alarm(myalarm,
                                                                     hour.get(),
                                                                     minute.get(),
                                                                     'gold',
                                                                     True))
            saveB.grid(row=2, column=1)
        
            self.toplevel.protocol('WM_DELETE_WINDOW', lambda: self.close_alarm(frame, myalarm, '-', '-'))

            self.myclock.init_alarm(self, "alarme1")

    def close_alarm(self, myalarm, hour, minute, color, state):
        #self.toplevel.destroy()
        self.isbottom = False
        myalarm.configure(text='Alarme 1 : {}:{}'.format(hour, minute))
        myalarm.configure(bg=color)
        self.alval['hour'] = hour
        self.alval['minute'] = minute
        self.alval['state'] = state
        self.myclock.init_alarm(self, "alarme1")
