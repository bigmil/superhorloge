import configparser

class Config:
    '''
    classe qui lit le config file et retourne un configparser
    prend 1 argument: le nom du fichier de configuration avec le chemin complet
    '''
    def __new__(cls, filename):
        '''
        lit le fichier de configuration et retourne un objet configparser
        prend 1 argument: filename
        '''
        config = configparser.ConfigParser()
        config.sections()
        config.read(filename)
        return(config)